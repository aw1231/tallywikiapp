package org.tallahasseewiki.mobile; // The name of the package
import java.io.IOException; // for IO errors
import java.io.InputStream; // allows for input from a stream
import java.io.InputStreamReader; //reads an input stream into a variable
import java.io.Reader; // top of reader
import java.net.HttpURLConnection; // creates HTTP connections
import java.net.URL; // variable for URLS
import android.net.ConnectivityManager; //The section of the code that manages connections
import android.net.NetworkInfo; // This section gives the app the information about its connection state
import android.os.Bundle; // information compressor
import android.app.Activity; // The section of code that displays stuff on the screen
import android.content.Context; // Gives various info about device
import android.text.method.ScrollingMovementMethod;
import android.util.Log; // allows app to make a log
import android.view.Menu; // menu code for if a user presses menu button
import android.widget.TextView;
import android.widget.Toast; // code for making toast messages
public class TallyMain extends Activity { // starts the main activity
	private static final String DEBUG_TAG = "TallyWiki"; // debug tag
    protected void onCreate(Bundle savedInstanceState) {  // runs on the start of the app
        super.onCreate(savedInstanceState); // loads any saved info (NOT USED)
        ConnectivityManager connMgr = (ConnectivityManager) // these two lines create a connection to the connectivity manger, a prereq for getting netowrk info
                getSystemService(Context.CONNECTIVITY_SERVICE); // see above
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo(); // creates a connection to the network info
            if (networkInfo != null && networkInfo.isConnected()) {  // checks if its connected to the network
            	setContentView(R.layout.activity_tally_loading); // displays loading spinning thing
			    try {
			    	
			    	String xml = downloadUrl("http://www.tallahasseewiki.org/api/page/Front_Page?format=xml&username=aw1231&api_key=fa3539b55b348578e77b395e531fb69bcefb69f3"); 
					TextView textView = new TextView(this);
				    textView.setTextSize(12);
				    textView.setText(xml);
				    textView.setMovementMethod(new ScrollingMovementMethod());
				    // Set the text view as the activity layout
				    setContentView(textView);
				} catch (IOException e) {
					Context context = getApplicationContext(); // determines various info required for making a toast (mostly device screen size is pertinent here)
			    	CharSequence text = "Error Connecting!"; // creates error message variable
			    	int duration = Toast.LENGTH_LONG; // sets display time to long (about 5-10 seconds)
			    	Toast toast = Toast.makeText(context, text, duration); // creates toast object from earlier variables
			    	toast.show(); // displays toast
				}						
            } else { // otherwise
            	Context context = getApplicationContext(); // determines various info required for making a toast (mostly device screen size is pertinent here)
            	CharSequence text = "Cannot connect! Please connect and restart the app."; // creates error message variable
            	int duration = Toast.LENGTH_LONG; // sets display time to long (about 5-10 seconds)
            	Toast toast = Toast.makeText(context, text, duration); // creates toast object from earlier variables
            	toast.show(); // displays toast
            } // ends else statement
    } // ends code for app 
    public boolean onCreateOptionsMenu(Menu menu) { // starts code for menu
        getMenuInflater().inflate(R.menu.activity_tally_main, menu); // Inflate the menu; this adds items to the action bar if it is present.
        return true; // returns true when complete
    } //closes menu code   
//Given a URL, establishes an HttpUrlConnection and retrieves
//the web page content as a InputStream, which it returns as
//a string.
private String downloadUrl(String requrl) throws IOException { // starts download code
 InputStream is = null; // creates an input stream 
 try { // used to prepare for any access errors
     URL url = new URL(requrl); // sets the url to the url that was passed to this method
     HttpURLConnection conn = (HttpURLConnection) url.openConnection(); //creates a connection to the said URL
     conn.setReadTimeout(10000 /* milliseconds */); // sets timeout to read the page
     conn.setConnectTimeout(15000 /* milliseconds */); // sets connection timeout
     conn.setRequestMethod("GET"); // sets connection type to a get
     conn.setDoInput(true); //  ??
     conn.connect();  // connects
     int response = conn.getResponseCode();  // creates variable with any error codes
     Log.d(DEBUG_TAG, "The response is: " + response); // logs error codes
     is = conn.getInputStream(); // reads data into variable
     // Convert the InputStream into a string
     Reader reader = null;
     reader = new InputStreamReader(is, "UTF-8");        
     char[] buffer = new char[50000];
     reader.read(buffer);
     String contentAsString = new String(buffer);
     return contentAsString;
     
 // Makes sure that the InputStream is closed after the app is
 // finished using it.
 }  catch (IOException e) {
		Context context = getApplicationContext(); // determines various info required for making a toast (mostly device screen size is pertinent here)
    	CharSequence text = "Error Connecting!"; // creates error message variable
    	int duration = Toast.LENGTH_LONG; // sets display time to long (about 5-10 seconds)
    	Toast toast = Toast.makeText(context, text, duration); // creates toast object from earlier variables
    	toast.show(); // displays toast
    	return "ERROR!";
	}
 finally {
     if (is != null) {
         is.close();
     } 
  }
 }

    

} // ends activity code